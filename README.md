# Rynx Android Web View Host

Simple Android Web View Host with Advance Features

## Features
- Build in permission
  - CAMERA
  - READ EXTERNAL STORAGE
  - WRITE EXTERNAL STORAGE
- Build in Camera, Gallery & File Chooser
  - Using [Pick IT](https://github.com/HBiSoft/PickiT/)
  - Using [Gligar](https://github.com/OpenSooq/Gligar)
- Build in Permission Check
- Build in Javascript Interfaces
- Build in Simple Notification
- Convert Default Alert JS To Beautiful Bottom Sheet
- Convert Default Confirm JS To Beautiful Bottom Sheet

## Usage
To clone this web host to your own Android App, please follow below steps:
1. Create An Android Project
2. Add below code to global build.gradle repositories

```gradle
repositories{
    ...
    mavenCentral()
    maven { url 'https://jitpack.io' }
}
```

3. Add below code to app build.gradle

```gradle
android {
    ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    dataBinding {
        enabled = true
    }
 }
 def butterKnifeVersion='10.2.1'
 dependencies {
    implementation "com.jakewharton:butterknife:${butterKnifeVersion}"
    annotationProcessor "com.jakewharton:butterknife-compiler:${butterKnifeVersion}"
    implementation 'com.opensooq.supernova:gligar:1.0.0'
    implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.0.0'
    implementation 'com.google.android.material:material:1.2.0-alpha05'
    implementation 'com.github.marcoscgdev:DialogSheet:2.0.9'
    implementation 'com.github.HBiSoft:PickiT:0.1.10'
    implementation 'com.karumi:dexter:6.0.2'
    ...
}
```

4. Copy and paste this file into your res/layout folder
   - activity_main.xml
   - dialogsheet_alert.xml
   - dialogsheet_alert_chooser.xml
5. Insert permission into your manifest

```xml
<manifest>
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.CAMERA"/>
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
 </manifest>
```

6. Copy and paste below .java files and resolve package name and context
   - MainActivity.java
   - utils/RynxJSAndroidBridge.java
   - utils/RynxSimpleNotification.java
   - Rename package example
   ```java
   package your.app.id; //before package dev.rynx.webview;
   ```
   - Fixing context example
   ```java
   import your.app.id.utils.RynxJSAndroidBridge;
   import your.app.id.utils.RynxSimpleNotification;
   import your.app.id.R;
   ```
   - Or you can simply find dev.rynx.webview and replace with you.app.id


## Javascript Interface Usage
### Available Interfaces
- func sendNotification(@NonNull String title, @NonNull String message, @Nullable String jsonStringifyData)
- func showToast(@NonNull String message,int duration)
- func setEnableSwipeRefreshLayout(boolean isEnabled)
- bool getEnableSwipeRefreshLayout()
```javascript
RynxJSAndroidBridge.funcName();
//example
if (RynxJSAndroidBridge!=null) {
    RynxJSAndroidBridge.sendNotification(title,message,jsonStringifyData);
}
```