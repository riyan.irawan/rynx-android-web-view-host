package dev.rynx.webview.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import dev.rynx.webview.MainActivity;
import dev.rynx.webview.R;

public class RynxSimpleNotification {
    private Context context;
    private String notificationChannel=null;
    private int notificationID;
    public RynxSimpleNotification(@NonNull Context context){
        super();
        this.context=context;
    }
    public RynxSimpleNotification setNotificationChannel(int notificationID, @NonNull String notificationChannel) {
        this.notificationChannel=notificationChannel;
        this.notificationID=notificationID;
        return this;
    }
    public RynxSimpleNotification build(){
        if(notificationChannel!=null) createNotificationChannel();
        return this;
    }

    private void createNotificationChannel(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = notificationChannel;
            String description = "Notification for "+notificationChannel;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(notificationChannel, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if(notificationManager!=null) notificationManager.createNotificationChannel(channel);
        }
    }

    public void sendNotification(@NonNull String title,@NonNull String text, @Nullable String jsonStringifyData) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,notificationChannel)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        Intent intent=new Intent(context,MainActivity.class);
        intent.putExtra("jsonStringifyData",jsonStringifyData);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent= PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        notificationID+=1;
        NotificationManagerCompat.from(context).notify(notificationID, builder.build());
    }
}
