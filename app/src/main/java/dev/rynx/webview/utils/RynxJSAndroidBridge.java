package dev.rynx.webview.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.marcoscg.dialogsheet.DialogSheet;

import dev.rynx.webview.R;

public class RynxJSAndroidBridge {
    private Context context;
    private WebView webView;
    public interface FileChooserCallback{
        boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams);
        void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType);
        void openFileChooser(ValueCallback<Uri> uploadMsg);
        void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture);

    }
    public interface LoadCallback {
        void onLoadError(WebView webView);
        void onLoadSuccess(WebView webView);
    }
    private FileChooserCallback fileChooserCallback;
    private LoadCallback loadCallback;
    private boolean isLoadError=false;

    public RynxJSAndroidBridge(@NonNull Context context,@NonNull WebView webView){
        super();
        this.context=context;
        this.webView=webView;
        setUpWebChromeClient();
        setUpWebClient();
    }
    public RynxJSAndroidBridge setFileChooserCallback(@NonNull FileChooserCallback callback){
        fileChooserCallback=callback;
        return this;
    }
    public RynxJSAndroidBridge setOnLoadError(@NonNull LoadCallback callback){
        loadCallback =callback;
        return this;
    }
    private void setUpWebChromeClient(){
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                DialogSheet dialogSheet=new DialogSheet(context)
                        .setBackgroundColor(Color.WHITE)
                        .setTitle("Informasi")
                        .setMessage(message)
                        .setView(R.layout.dialogsheet_alert)
                        .setColoredNavigationBar(true)
                        .setRoundedCorners(true)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogSheet.OnPositiveClickListener() {
                            @Override
                            public void onClick(View v) {
                                result.confirm();
                            }
                        })
                        .setButtonsColorRes(R.color.colorAccent) // You can use dialogSheetAccent style attribute instead
                        ;
                View viewDialog=dialogSheet.getInflatedView();
                ImageView img = viewDialog.findViewById(R.id.dialogsheet_alert_img);
                img.setImageResource(R.drawable.undraw_online_information_4ui6);
                dialogSheet.show();

                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                DialogSheet dialogSheet=new DialogSheet(context)
                        .setBackgroundColor(Color.WHITE)
                        .setView(R.layout.dialogsheet_alert)
                        .setColoredNavigationBar(true)
                        .setRoundedCorners(true)
                        .setCancelable(false)
                        .setTitle("Konfirmasi")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, new DialogSheet.OnPositiveClickListener() {
                            @Override
                            public void onClick(View v) {
                                result.confirm();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogSheet.OnNegativeClickListener() {
                            @Override
                            public void onClick(View v) {
                                result.cancel();
                            }
                        })
                        .setButtonsColorRes(R.color.colorAccent) // You can use dialogSheetAccent style attribute instead
                        ;
                View viewDialog=dialogSheet.getInflatedView();
                ImageView img = viewDialog.findViewById(R.id.dialogsheet_alert_img);
                img.setImageResource(R.drawable.undraw_confirmed_81ex);
                dialogSheet.show();
                return true;
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.e("CONSOLE BRIDGE",consoleMessage.message());
                return true;
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                return fileChooserCallback.onShowFileChooser(webView,filePathCallback,fileChooserParams);
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType){
                fileChooserCallback.openFileChooser(uploadMsg,acceptType);
            }
            public void openFileChooser(ValueCallback<Uri> uploadMsg){
                fileChooserCallback.openFileChooser(uploadMsg);
            }

            public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture){
                fileChooserCallback.openFileChooser(filePathCallback,acceptType,capture);
            }
        });
    }

    private void setUpWebClient(){
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                isLoadError=true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                isLoadError=true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                isLoadError=false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(isLoadError){
                    loadCallback.onLoadError(view);
                }
                else{
                    loadCallback.onLoadSuccess(view);
                }
            }
        });
    }

    public RynxJSAndroidBridge withRynxJSInterface(JSInterfaces interfaces){
        webView.addJavascriptInterface(interfaces,"RynxJSAndroidBridge");
        return this;
    }

    public WebView init(){
        return webView;
    }

    // JS Interface
    public static class JSInterfaces{

        public interface NotificationCallback{
            void onNotificationSend(@NonNull String title, @NonNull String message, @Nullable String jsonStringifyData);
        }

        public interface RefreshLayoutChangeCallback{
            void onEnabledChanged(boolean isEnabled);
            boolean getEnableStatus();
        }
        private Context context;
        private NotificationCallback notificationCallback;
        private RefreshLayoutChangeCallback refreshLayoutChangeCallback;


        public JSInterfaces(@NonNull Context context){
            super();
            this.context=context;
        }
        public JSInterfaces setupNotification(@NonNull NotificationCallback callback){
            notificationCallback=callback;
            return this;
        }
        public JSInterfaces setupSwipeRefreshLayout(@NonNull RefreshLayoutChangeCallback callback){
            refreshLayoutChangeCallback=callback;
            return this;
        }

        @JavascriptInterface
        public void sendNotification(@NonNull String title, @NonNull String message, @Nullable String jsonStringifyData){
            notificationCallback.onNotificationSend(title,message,jsonStringifyData);
        }

        @JavascriptInterface
        public void showToast(@NonNull String message,int duration){
            Toast.makeText(context,message,duration).show();
        }

        @JavascriptInterface
        public void setEnableSwipeRefreshLayout(boolean isEnabled){
            refreshLayoutChangeCallback.onEnabledChanged(isEnabled);
        }

        @JavascriptInterface
        public boolean getEnableSwipeRefreshLayout(){
            return refreshLayoutChangeCallback.getEnableStatus();
        }
    }
}
