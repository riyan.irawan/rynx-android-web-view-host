package dev.rynx.webview;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.hbisoft.pickit.PickiT;
import com.hbisoft.pickit.PickiTCallbacks;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.marcoscg.dialogsheet.DialogSheet;
import com.opensooq.supernova.gligar.GligarPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.rynx.webview.utils.RynxJSAndroidBridge;
import dev.rynx.webview.utils.RynxSimpleNotification;

@SuppressWarnings("setJavaScriptEnabled")
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_refresh_layout)
    SwipeRefreshLayout srlMain;
    @BindView(R.id.main_web_view_container)
    WebView webViewMain;
    @BindView(R.id.no_connection_img)
    ImageView noConnectionView;

    private int exitBackCounter=1;
    private RynxSimpleNotification rynxSimpleNotification;
    private final int IMAGE_CHOOSER_CODE=77;
    private final int FILE_CHOOSER_CODE=88;
    private ValueCallback<Uri[]> filePathCalls;
    private ValueCallback<Uri> upload;
    private PickiT pickiT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(getSupportActionBar()!=null) getSupportActionBar().hide();

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getNotificationData(getIntent());
        srlMain.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary,R.color.colorPrimaryDark);
        srlMain.setOnRefreshListener(() -> {
            webViewMain.reload();
            srlMain.setRefreshing(false);
        });
        int notificationID = 1;
        String notificationChannel = "Rynx77";
        rynxSimpleNotification=new RynxSimpleNotification(this)
                .setNotificationChannel(notificationID, notificationChannel)
                .build();

    }

    private void getNotificationData(Intent intent){
        String jsonData=intent.getStringExtra("jsonStringifyData");
        if(jsonData!=null){
            try{
                JSONObject jsonObject=new JSONObject(jsonData);
                String url=jsonObject.getString("url");
                if ( webViewMain!=null ) webViewMain.loadUrl(url);
            }catch (JSONException e){
                if (e.getMessage()!=null) Log.e("JSON",e.getMessage());
            }
        }
        else{
            setUpWebView();
        }
    }

    private void clearFileChooser(){

        if(filePathCalls!=null){
            filePathCalls.onReceiveValue(new Uri[]{});
            filePathCalls=null;
        }
        if(upload!=null) {
            upload.onReceiveValue(null);
            upload=null;
        }
    }

    private void showFileChooser(){

        Dexter.withActivity(MainActivity.this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            DialogSheet dialogSheet=new DialogSheet(MainActivity.this)
                                    .setView(R.layout.dialogsheet_alert_chooser)
                                    .setBackgroundColor(Color.WHITE)
                                    .setTitle("Choose File")
                                    .setRoundedCorners(true)
                                    .setCancelable(false)
                                    .setNegativeButton("Cancel",(v)->{
                                        if(filePathCalls!=null) filePathCalls.onReceiveValue(new Uri[]{});
                                        filePathCalls=null;
                                        if(upload!=null) upload.onReceiveValue(null);
                                        upload=null;
                                    });
                            View v=dialogSheet.getInflatedView();
                            LinearLayout cameraChooser=v.findViewById(R.id.dialogsheet_alert_camera_chooser);
                            cameraChooser.setOnClickListener((view)->{
                                dialogSheet.dismiss();
                                new GligarPicker().requestCode(IMAGE_CHOOSER_CODE)
                                        .withActivity(MainActivity.this)
                                        .limit(1)
                                        .show();
                            });
                            LinearLayout fileChooser=v.findViewById(R.id.dialogsheet_alert_file_chooser);
                            fileChooser.setOnClickListener((view)->{

                                pickiT=new PickiT(MainActivity.this, new PickiTCallbacks() {
                                    @Override
                                    public void PickiTonStartListener() {

                                    }

                                    @Override
                                    public void PickiTonProgressUpdate(int progress) {

                                    }

                                    @Override
                                    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {

                                        path="file://"+path;
                                        if(filePathCalls!=null)
                                        {
                                            filePathCalls.onReceiveValue(new Uri[]{Uri.parse(path)});
                                            filePathCalls=null;
                                        }

                                        if(upload!=null){
                                            upload.onReceiveValue(Uri.parse(path));
                                            upload=null;
                                        }
                                    }
                                });
                                dialogSheet.dismiss();
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                intent.setType("file/*");
                                startActivityForResult(intent, FILE_CHOOSER_CODE);
                            });
                            dialogSheet.show();
                        }
                        else{
                            Toast.makeText(MainActivity.this,"You have to granted all permission!",Toast.LENGTH_LONG).show();
                            clearFileChooser();
                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        clearFileChooser();
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(error-> Log.e("DEXTER",error.toString()))
                .check();


    }

    private void setUpWebView(){
        WebSettings settings =webViewMain.getSettings();
        settings.setJavaScriptEnabled(true);
        if(Build.VERSION.SDK_INT>=26) settings.setSafeBrowsingEnabled(false);
        settings.setDomStorageEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setLoadWithOverviewMode(true);
        settings.setSupportZoom(false);
        settings.setUseWideViewPort(true);

        webViewMain = new RynxJSAndroidBridge(MainActivity.this,webViewMain)
                .withRynxJSInterface(
                        new RynxJSAndroidBridge
                                .JSInterfaces(MainActivity.this)
                                .setupNotification(((title, message, jsonStringifyData) -> rynxSimpleNotification.sendNotification(title,message,jsonStringifyData)))
                                .setupSwipeRefreshLayout(new RynxJSAndroidBridge.JSInterfaces.RefreshLayoutChangeCallback() {
                                    @Override
                                    public void onEnabledChanged(boolean isEnabled) {
                                        runOnUiThread(()-> {
                                            if(srlMain.isEnabled()!=isEnabled) srlMain.setEnabled(isEnabled);
                                        });
                                    }

                                    @Override
                                    public boolean getEnableStatus() {
                                        return srlMain.isEnabled();
                                    }
                                })
                )
                .setFileChooserCallback(new RynxJSAndroidBridge.FileChooserCallback() {
                    @Override
                    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                        filePathCalls=filePathCallback;
                        showFileChooser();
                        return true;
                    }

                    @Override
                    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                        upload=uploadMsg;
                        showFileChooser();

                    }

                    @Override
                    public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                        upload=uploadMsg;
                        showFileChooser();

                    }
                    @Override
                    public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture) {
                        upload=filePathCallback;
                        showFileChooser();
                    }
                })
                .setOnLoadError(new RynxJSAndroidBridge.LoadCallback() {
                    @Override
                    public void onLoadError(WebView webView) {
                        runOnUiThread(()->{
                            if(webView.isShown()){
                                webView.setVisibility(View.GONE);
                                noConnectionView.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onLoadSuccess(WebView webView) {
                        runOnUiThread(()->{
                            if(!webView.isShown()){
                                webView.setVisibility(View.VISIBLE);
                                noConnectionView.setVisibility(View.GONE);
                            }
                        });
                    }
                })
                .init();
        String loadedURL = "https://mobilekoperasiumkmdev.bgrlogistics.id";
        webViewMain.loadUrl(loadedURL);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        getNotificationData(intent);
        super.onNewIntent(intent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case IMAGE_CHOOSER_CODE : {

                if(data!=null && data.getExtras()!=null){
                    String[] pathsList= data.getExtras().getStringArray(GligarPicker.IMAGES_RESULT); // return list of selected images paths.
                    if(pathsList!=null && pathsList.length>0){
                        if(filePathCalls!=null)
                            filePathCalls.onReceiveValue(new Uri[]{Uri.parse("file://"+pathsList[0])});
                            filePathCalls=null;
                        if(upload!=null)
                            upload.onReceiveValue(Uri.parse("file://"+pathsList[0]));
                            upload=null;
                    }
                    else{
                        clearFileChooser();
                    }
                }
                else{
                    clearFileChooser();
                }

                break;
            }
            case FILE_CHOOSER_CODE:{
                if(data!=null && data.getData()!=null){
                    pickiT.getPath(data.getData(),Build.VERSION.SDK_INT);
                }
                else{
                    clearFileChooser();
                }
                break;
            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed() {
        if(webViewMain.canGoBack()){
            webViewMain.goBack();
        }
        else{
            if(exitBackCounter<2)
            {
                Toast.makeText(this,"Klik kembali sekali lagi untuk keluar",Toast.LENGTH_SHORT).show();
                exitBackCounter+=1;
            }
            else{
                super.onBackPressed();
                if(pickiT!=null) pickiT.deleteTemporaryFile();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations()) {
            if(pickiT!=null) pickiT.deleteTemporaryFile();
        }
    }
}
